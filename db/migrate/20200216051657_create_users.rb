class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :email, null: false
      t.string :password_digest, null: false
      t.boolean :delete_flag, default: false

      t.timestamps
      #indexを設定することでデータの数が多くなって来たときにも検索を高速で行えるようになる。
      #本に挟む付箋のようなもの。どのカラムにも貼ればいいものではなく、参照される機会が多いものに設定すると良い。
      t.index :email, unique: true
    end
  end
end