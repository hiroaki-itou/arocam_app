class AddUserIdToMemos < ActiveRecord::Migration[5.2]
  # マイグレーションファイルのメソッドにはchange以外にもup/downメソッドがある。
  # up・・・実際に変更する内容を記載（カラムの追加、削除等）
  # down・・upした内容を元に戻す(rollbackする)際の処理を記載（upでカラムの追加をしたならdownでは削除）

  # change・・upに記載するような内容だけ書いておけばrollbackの内容も自動推測してくれるが、あくまで推測なのでrollback時に
  # 　　　　　　意図した状態に戻らないことがある。upとdownに分けて記載しておく方がベター。

def up
   # 今存在するmemoテーブルのレコードを全削除する。※SQL上で行っても良い。
  execute 'DELETE FROM memos;'

  # add_reference テーブル名, リファレンス名(参照するテーブル名), オプション
  # memosテーブルにusersテーブルの情報を参照する(どのユーザーから作られたメモか区別する)ためのidを定義する。 
  # referenceは他のテーブルを参照するためのidを作成してくれる。 foreign_keyの制約はmemoが作成されるときに存在しないuser_idが入るのを防いでくれる。
  add_reference :memos, :user, foreign_key: true, null: false, index: true
end

def down
  remove_foreign_key :mamos, :users
  remove_reference :memos, :user, index: true
end

# def change
#   今回はchangeは使わないが、書くならこんな感じ
#  execute 'DELETE FROM memos;'
#  add_reference :memos, :user, foreign_key: true, null: false, index: true
# end
end