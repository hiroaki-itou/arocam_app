Rails.application.routes.draw do
  resources :memos do
    post :import, on: :collection
  end
  root to: 'memos#index'
  resources :users, only: [:new, :create]
  
  get '/login', to: 'sessions#new'
  post '/login',  to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  # get 'memos/index'
  # get 'memos/show'
  # get 'memos/new'
  # get 'memos/edit'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
