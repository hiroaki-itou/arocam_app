require 'rails_helper'

describe 'メモ管理', type: :system do
    describe '一覧表示機能' do
        # テスト実施前の事前準備
        before do
            #ユーザーを作成しておく(users.rbで定義したuser情報を作成する。)
            user1 = FactoryBot.create(:user, user_name: 'ユーザー1', email: 'user1@example.com')
            #作成者がユーザーAであるメモを作成しておく
            FactoryBot.create(:memo, title: 'user1のメモ', memo_text: "テキストテキストテキスト", user: user1)
        end

        context 'ユーザー1がログインしている時' do
            before do
                #ユーザー1でログインする。
                visit login_path
                fill_in "session[email]", with: 'user1@example.com'
                fill_in "session[password]", with:'password'
                click_button 'ログインする'
            end
            
            it 'ユーザー1が作成したメモが表示されること' do
                # 期待する結果を記載
                expect(page).to have_content 'user1のメモ'
            end
        end

    end
end