FactoryBot.define do
    factory :memo do
        title { 'Ruby' }
        memo_text { 'Rspecの勉強' }
        user #関連するuserの設定がされていればこの記載だけでuser_idを推測してくれる。
    end
end