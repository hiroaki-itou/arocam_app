FactoryBot.define do
    factory :user do
        user_name { 'factoryユーザー' }
        email { 'factory@example.com' }
        password { 'password' }
    end
end