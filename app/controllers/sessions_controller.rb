class SessionsController < ApplicationController
  include SessionsHelper

  def new
  end

  def create
    # 入力されたメールアドレスに該当するユーザーを取得する。
    user = User.find_by(email: session_params[:email])

    # authenticateメソッドは、入力されたパスワードをハッシュ化し。
    # usersテーブルに保存されているパスワード（既にハッシュ化されているもの）を比較して一致するかどうかを判定する。
    # &.は「ぼっちメソッド」とも呼ばれ。直前に書いているオブジェクトが空(nil)の場合を許容する（エラーを握り潰す)※nilを許容できないものには使用しないこと。
    if user&.authenticate(session_params[:password])
      # セッション変数にログイン成功したユーザーのIDを格納している。セッションが切れるまで保持され続ける。
      login(user)
      redirect_to root_path, notice: 'ログインしました'
    else
      render :new
    end
  end

  def destroy
    session.delete(:user_id)
    redirect_to root_path, notice: 'ログアウトしました'
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end

end