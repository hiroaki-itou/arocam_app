class ApplicationController < ActionController::Base
    #ヘルパーメソッドを定義することで全てのビューから使える。
    helper_method :current_user 

    private

    def current_user
        # session変数にログインユーザーのIDが存在して、なおかつ@current_userの中身が nil の場合。
        # session変数のユーザーIDと一致するユーザーの情報を@current_userの中に格納する。
        # ||=はnilガード、左辺の変数が空の時だけ値を代入して、すでに値があれば何もしない。
        @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id] #後置if session[:id]の中身が存在するかどうかを判定。
    end
end
