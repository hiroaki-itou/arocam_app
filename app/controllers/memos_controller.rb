class MemosController < ApplicationController
  before_action :login_required

  def index
    # @memos = Memo.all
    @memos = current_user.memos.all.page(params[:page]).per(10) #こう (10件区切りで表示するように指定)

    #リクエストされた形式によって画面（html）を表示する場合とCSVファイルを出力する場合を分けている。
    respond_to do |format|
      format.html
      #CSV形式で出力された場合のファイル名を指定
      format.csv { send_data @memos.generate_csv, filename: "memos-#{Time.zone.now.strftime('%Y%m%d%S')}.csv" }
    end
  end

  def show
    @memos = current_user.memos.all.page(params[:page]).per(10)
  end

  def new
    @memo = Memo.new
  end

  def create
    @memo = current_user.memos.new(memo_params)
    if @memo.save
      redirect_to root_path, notice: "メモ「#{@memo.title}」を登録しました。"
    else
      render :new
    end
  end

  def edit
    @memo = Memo.find(params[:id])
  end

  def update
    memo = Memo.find(params[:id])
    memo.update!(memo_params)
    redirect_to root_path, notice: "メモ「#{memo.title}」を編集しました。"
  end

  def destroy
    memo = Memo.find(params[:id])
    memo.destroy
    redirect_to root_path, notice: "メモ「#{memo.title}」を削除しました。"
  end

  def import
    current_user.memos.import(params[:file])
    redirect_to root_path, notice: "CSVインポートを行いました。"
  end

  private
  def memo_params
    params.require(:memo).permit(:title, :memo_text)
  end

  def login_required
    redirect_to login_path unless current_user
   end
end
