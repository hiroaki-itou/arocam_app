class Memo < ApplicationRecord
    belongs_to :user

    # CSV出力する項目の順番を指定するための配列を作成するメソッド
    def self.csv_attributes
        [:title, :memo_text, :created_at, :updated_at, :user_id]
    end

    def self.generate_csv
        CSV.generate(headers: true) do | csv |
            #self.csv_attributesで作成した配列の順番でcsvのヘッダー（項目名を作成）
            csv << csv_attributes
            # memosテーブルのレコードを一行ずつファイルに出力している。
            all.each do | memo |
                csv << csv_attributes.map{ |attr| memo.send(attr) }
            end
        end
    end

    def self.import(file)
        # ファイルを一行ずつ読み込むためのループ、一行目はヘッダーとしての行なので読み込まないように指定
        CSV.foreach(file.path, headers: true) do |row|
            # memo = new という省略記法もできる。
            memo = self.new
            #ヘッダーの項目に対応する値を取得している
            memo.attributes = row.to_hash
            memo.save!
        end
    end
end
